import { BrowserModule } from '@angular/platform-browser';

import { LOCALE_ID, NgModule } from '@angular/core';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ElementoMenuComponent } from './components/common/elemento-menu/elemento-menu.component';
import {FormsModule} from '@angular/forms';
import { DetalleComponent } from './components/common/detalle/detalle.component';
import { UpperwordPipe } from './pipes/upperword.pipe';
import { AboutComponent } from './components/pages/about/about.component';
import { DataComponent } from './components/pages/data/data.component';
import { InfoComponent } from './components/pages/info/info.component';

registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ElementoMenuComponent,
    DetalleComponent,
    UpperwordPipe,
    AboutComponent,
    DataComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
