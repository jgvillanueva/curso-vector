/*
menu.component.ts
 */


import { Component, OnInit } from '@angular/core';
import {UtilService} from '../services/util.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  _time = '';
  get time(): string {
    return this._time;
  }
  set time(valor: string) {
    this._time = 'Milisegundos: ' + valor;
  }

  valorescrito;

  listaDatos = [
    { texto: 'Mis ahorros', value: 302342300 },
    { texto: 'Mis deudas', value: 12.234 },
    { texto: 'Mis ingresos', value: 234 },
    { texto: 'Mis impuestos', value: 2333003 },
  ];

  constructor(
    private utilsService: UtilService,
  ) { }

  ngOnInit(): void {
    setInterval(() => {
      this.time = Date.now().toString();
    }, 1000);
  }

  isEven(index: number): boolean {
    return index % 2 === 0;
  }

  onSelect(valor): void {
    this.valorescrito = valor;
    // ToDo: add setValor
    this.utilsService.setValor(valor);
  }

  onModelChange(): void{
    this.utilsService.setValor(this.valorescrito);
  }
}
