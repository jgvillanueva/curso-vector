import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit, OnDestroy {
  type;
  datos;
  paramsSubscription: Subscription;

  constructor(
    private miRuta: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.paramsSubscription = this.miRuta.paramMap.subscribe(params => {
      this.type = params.get('type');
      this.datos = history.state.datos;
    });
  }

  ngOnDestroy(): void{
    this.paramsSubscription.unsubscribe();
  }

}
