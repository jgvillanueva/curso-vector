/*
detalle.component.ts
 */

import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../../services/util.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit {
  valorescrito = 'hola';

  constructor(
    public utilsService: UtilService
  ) { }

  ngOnInit(): void {
  }

}
