import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-elemento-menu',
  templateUrl: './elemento-menu.component.html',
  styleUrls: ['./elemento-menu.component.scss']
})
export class ElementoMenuComponent implements OnInit, OnChanges {

  @Input()
  texto: string;

  @Output()
  seleccion = new EventEmitter<number>();

  @Input()
  value: number;

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(): void {
    this.seleccion.emit(this.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { texto } = changes;
    if (texto.currentValue !== texto.previousValue) {
      console.log('Valor de texto cambiado');
    }
  }
}
