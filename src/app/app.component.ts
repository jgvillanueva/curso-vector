import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-root', // selector usado para añadir este componente como etiqueta
  templateUrl: './app.component.html', // html que define el markup del componente
  styleUrls: ['./app.component.scss'] // array con las urls de los scss asociados al componente
})
export class AppComponent {
  title = 'Curso Ionic';

  constructor() {
  }


}
