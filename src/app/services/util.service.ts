import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  valorescrito: number;

  constructor() { }

  setValor(valor: number): void {
    this.valorescrito = valor;
  }

  getValor(): number {
    return Number(this.valorescrito);
  }
}
