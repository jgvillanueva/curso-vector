import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  datos = [
    { type: 'Email', valor: 'jorgegvillanueva@gmail.com' },
    { type: 'Teléfono', valor: '666666666' },
    { type: 'Dirección', valor: 'Gran Vía 23' },
  ];
  constructor(
    private router: Router,
    private miRuta: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

  goData(dato) {
    this.router.navigate(['data/' + dato.type], {
      state: { datos: dato },
      relativeTo: this.miRuta
    });
  }

}
